package com.tw.flightbook.controller;

import com.tw.flightbook.models.Flight;
import com.tw.flightbook.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


@RestController
public class FlightController {

    private FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping("/flights")
    public Collection<Flight> flights()
    {
        return flightService.getAllFlights();
    }

    @GetMapping("/flights/search")
    public Collection<Flight> searchAvailableFlightsFlights(@RequestParam("HYD") String source,
                                            @RequestParam("LOSANGELES") String destination,
                                            @RequestParam(value = "count", defaultValue = "1") int passengerCount)
    {
        return flightService.searchAvailableFlights(source,destination,passengerCount);
    }
}
