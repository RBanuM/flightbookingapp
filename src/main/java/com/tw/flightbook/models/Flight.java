package com.tw.flightbook.models;

//import jdk.nashorn.internal.objects.annotations.Getter;

import java.util.ArrayList;
import java.util.HashMap;

public class Flight {

        private String flightName;
        private String source;
        private String destination;
        private int firstClassNoSeats;
        private double firstClassBaseFare;
        private int businessClassNoSeats;
        private double businessClassBaseFare;
        private int economyClassNoSeats;
        private double economyClassBaseFare;

        public Flight(String flightName, String source, String destination, int firstClassNoSeats, double firstClassBaseFare, int businessClassNoSeats, double businessClassBaseFare, int economyClassNoSeats, double economyClassBaseFare) {
                this.flightName = flightName;
                this.source = source;
                this.destination = destination;
                this.firstClassNoSeats = firstClassNoSeats;
                this.firstClassBaseFare = firstClassBaseFare;
                this.businessClassNoSeats = businessClassNoSeats;
                this.businessClassBaseFare = businessClassBaseFare;
                this.economyClassNoSeats = economyClassNoSeats;
                this.economyClassBaseFare = economyClassBaseFare;
        }
        public String getFlightName() {
                return flightName;
        }

        public void setFlightName(String flightName) {
                this.flightName = flightName;
        }

        public String getSource() {
                return source;
        }

        public void setSource(String source) {
                this.source = source;
        }

        public String getDestination() {
                return destination;
        }

        public void setDestination(String destination) {
                this.destination = destination;
        }

        public int getFirstClassNoSeats() {
                return firstClassNoSeats;
        }

        public void setFirstClassNoSeats(int firstClassNoSeats) {
                this.firstClassNoSeats = firstClassNoSeats;
        }

        public double getFirstClassBaseFare() {
                return firstClassBaseFare;
        }

        public void setFirstClassBaseFare(double firstClassBaseFare) {
                this.firstClassBaseFare = firstClassBaseFare;
        }

        public int getBusinessClassNoSeats() {
                return businessClassNoSeats;
        }

        public void setBusinessClassNoSeats(int businessClassNoSeats) {
                this.businessClassNoSeats = businessClassNoSeats;
        }

        public double getBusinessClassBaseFare() {
                return businessClassBaseFare;
        }

        public void setBusinessClassBaseFare(double businessClassBaseFare) {
                this.businessClassBaseFare = businessClassBaseFare;
        }

        public int getEconomyClassNoSeats() {
                return economyClassNoSeats;
        }

        public void setEconomyClassNoSeats(int economyClassNoSeats) {
                this.economyClassNoSeats = economyClassNoSeats;
        }

        public double getEconomyClassBaseFare() {
                return economyClassBaseFare;
        }

        public void setEconomyClassBaseFare(double economyClassBaseFare) {
                this.economyClassBaseFare = economyClassBaseFare;
        }

}
