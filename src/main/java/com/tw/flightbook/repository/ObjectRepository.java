package com.tw.flightbook.repository;

public interface ObjectRepository<T> {

    public void store(T t);

    public T retrieve(String key);

    public T search(String key);

    public T delete(String key);
}
