package com.tw.flightbook.repository;

import com.tw.flightbook.models.Flight;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;

@Repository
public class FlightRepository implements ObjectRepository<Flight>{

    private HashMap<String, Flight> FlightEntries = init();


    public HashMap<String, Flight> init() {
        FlightEntries = new HashMap<String, Flight>();

        FlightEntries.put("Boeing 777- 200LR(77L)", new Flight("Boeing 777- 200LR(77L)", "HYD", "LOSANGELES", 8, 20000D, 35, 13000D, 195, 6000D));
        FlightEntries.put("Airbus A319 V2", new Flight("Airbus A319 V2", "HYD", "SANFRANSISCO",0, 0D, 0, 0D, 144, 4000D));
        FlightEntries.put("Airbus A321", new Flight("Airbus A321", "HYD", "KANSAS",0, 0D, 20, 10000D, 152, 5000D));

        return FlightEntries;
    }

    public HashMap<String, Flight> handleToRepoMap()
    {
        return FlightEntries;
    }

    public Collection<Flight> getAllFlights()
    {
        return handleToRepoMap().values();
    }

    //Assuming flight name is the primary key

    public void store(Flight t)
    {
        FlightEntries.put(t.getFlightName(), t);
    }

    public Flight retrieve(String key)
    {
        return FlightEntries.get(key);
    }

    public Flight search(String key)
    {
        //if (FlightEntries.containsKey(key))
        return FlightEntries.get(key);
    }

    public Flight delete(String key)
    {
        Flight f = search(key);
        FlightEntries.remove(key);
        return f;
    }

}
