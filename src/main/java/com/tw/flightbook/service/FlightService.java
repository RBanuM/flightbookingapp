package com.tw.flightbook.service;

import com.tw.flightbook.models.Flight;
import com.tw.flightbook.repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class FlightService {

 private FlightRepository flightRepository;

    @Autowired
    public FlightService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public Collection<Flight> getAllFlights()
    {
        return flightRepository.getAllFlights();
    }

    public Collection<Flight> searchAvailableFlights(String source, String destination, int passengerCount)
    {
        return getAllFlights().stream()
                .filter(flight -> flight.getSource().equals(source) && flight.getDestination().equals(destination) && flightHasSeats(flight,passengerCount))
                .collect(Collectors.toList());
    }

    private boolean flightHasSeats(Flight flight, int seatCount)
    {
        int availableSeats = flight.getFirstClassNoSeats()+flight.getBusinessClassNoSeats()+flight.getEconomyClassNoSeats();
        return availableSeats >= seatCount;
    }

}
