package com.tw.flightbook.service;

import com.tw.flightbook.models.Flight;
import com.tw.flightbook.repository.FlightRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FlightServiceTest {
    FlightRepository flightRepository;
    FlightService flightService;
    @Before
    public void setUp()
    {
        flightRepository = mock(FlightRepository.class);
        flightService = new FlightService(flightRepository);
    }

    @Test
    public void shouldReturnAvailableFlights()
    {
        // given
        List<Flight> flights = new ArrayList<>();
        Flight flight1 =  new Flight("Boeing 777- 200LR(77L)", "HYD", "LOSANGELES", 8, 20000D, 35, 13000D, 195, 6000D);
        Flight flight2 = new Flight("Airbus A319 V2", "HYD", "SANFRANSISCO",0, 0D, 0, 0D, 144, 4000D);
        flights.add(flight1);
        flights.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flights);

        // when
        Collection<Flight> allFlights = flightService.getAllFlights();

        // then
        assertEquals(2,allFlights.size());
    }

    @Test
    public void shouldReturnZeroFlights()
    {
        // given
        List<Flight> flights = new ArrayList<>();
        when(flightRepository.getAllFlights()).thenReturn(flights);

        // when
        Collection<Flight> allFlights = flightService.getAllFlights();

        // then
        assertEquals(0,allFlights.size());
    }

    @Test
    public void shouldReturnMatchingFlights()
    {
        // given
        List<Flight> flights = new ArrayList<>();


        Flight flight1 =  new Flight("Boeing 777- 200LR(77L)", "HYD", "LOSANGELES", 8, 20000D, 35, 13000D, 195, 6000D);
        Flight flight2 = new Flight("Airbus A319 V2", "HYD", "SANFRANSISCO",0, 0D, 0, 0D, 144, 4000D);
        flights.add(flight1);
        flights.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flights);

        Collection<Flight> result = flightService.searchAvailableFlights("India", "UK", 2);
        assertEquals(1,result.size());
    }

    @Test
    public void shouldReturnZeroMatchingFlights()
    {
        // given
        List<Flight> flights = new ArrayList<>();
        Flight flight1 =  new Flight("Boeing 777- 200LR(77L)", "HYD", "LOSANGELES", 8, 20000D, 35, 13000D, 195, 6000D);
        Flight flight2 = new Flight("Airbus A319 V2", "HYD", "SANFRANSISCO",0, 0D, 0, 0D, 144, 4000D);
        flights.add(flight1);
        flights.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flights);

        Collection<Flight> result = flightService.searchAvailableFlights("India", "UK", 7);
        assertEquals(0,result.size());
    }

    @Test
    public void shouldReturnZeroMatchingFlightsWhenSourceDoesNotMatch()
    {
        // given
        List<Flight> flights = new ArrayList<>();
        Flight flight1 =  new Flight("Boeing 777- 200LR(77L)", "HYD", "LOSANGELES", 8, 20000D, 35, 13000D, 195, 6000D);
        Flight flight2 = new Flight("Airbus A319 V2", "HYD", "SANFRANSISCO",0, 0D, 0, 0D, 144, 4000D);
        flights.add(flight1);
        flights.add(flight2);
        when(flightRepository.getAllFlights()).thenReturn(flights);

        Collection<Flight> result = flightService.searchAvailableFlights("HYD", "LOSANGELES", 1);
        assertEquals(0,result.size());
    }

    @Test
    public void shouldReturnZeroMatchingFlightsWhenThereAreNoFlights()
    {
        // given
        List<Flight> flights = new ArrayList<>();
        when(flightRepository.getAllFlights()).thenReturn(flights);

        Collection<Flight> result = flightService.searchAvailableFlights("HYD", "BOSTON", 1);
        assertEquals(0,result.size());
    }

}
